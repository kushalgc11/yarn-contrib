"use strict";
/*
 * Copyright 2020 Larry1123
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReleaseTags = void 0;
const tslib_1 = require("tslib");
// imports
const core_1 = require("@yarnpkg/core");
const cli_1 = require("@yarnpkg/cli");
const versionUtils_1 = (0, tslib_1.__importDefault)(require("@yarnpkg/plugin-version/lib/versionUtils"));
const clipanion_1 = require("clipanion");
const options_1 = require("clipanion/lib/advanced/options");
class ReleaseTags extends clipanion_1.Command {
    constructor() {
        super(...arguments);
        this.json = (0, options_1.Boolean)(`--json`, false, { description: `Format the output as an NDJSON stream` });
        this.all = (0, options_1.Boolean)(`--all`, false, { description: `Apply the deferred version changes on all workspaces` });
    }
    async execute() {
        const configuration = await core_1.Configuration.find(this.context.cwd, this.context.plugins);
        const { project, workspace, } = await core_1.Project.find(configuration, this.context.cwd);
        if (!workspace) {
            throw new cli_1.WorkspaceRequiredError(project.cwd, this.context.cwd);
        }
        const releases = await versionUtils_1.default.resolveVersionFiles(project);
        console.dir(releases);
        return 0;
    }
}
exports.ReleaseTags = ReleaseTags;
ReleaseTags.paths = [[`release`, 'tags']];
ReleaseTags.usage = clipanion_1.Command.Usage({
    description: '',
    details: '',
    examples: [[``, ``]],
});
