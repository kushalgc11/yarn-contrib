PK     @��               node_modules/PK     @��               node_modules/@yarnpkg/PK     @��            "   node_modules/@yarnpkg/plugin-http/PK
     @�����   �   +   node_modules/@yarnpkg/plugin-http/README.md# `@yarnpkg/plugin-http`

This plugin adds support for downloading packages through HTTP.

## Install

This plugin is included by default in Yarn.
PK     @��            &   node_modules/@yarnpkg/plugin-http/lib/PK
     @��i��p�  �  =   node_modules/@yarnpkg/plugin-http/lib/TarballHttpFetcher.d.tsimport { Fetcher, FetchOptions, MinimalFetchOptions } from '@yarnpkg/core';
import { Locator } from '@yarnpkg/core';
export declare class TarballHttpFetcher implements Fetcher {
    supports(locator: Locator, opts: MinimalFetchOptions): boolean;
    getLocalPath(locator: Locator, opts: FetchOptions): null;
    fetch(locator: Locator, opts: FetchOptions): Promise<{
        packageFs: import("@yarnpkg/fslib").FakeFS<import("@yarnpkg/fslib").PortablePath>;
        releaseFs: () => void;
        prefixPath: import("@yarnpkg/fslib").PortablePath;
        checksum: string | null;
    }>;
    fetchFromNetwork(locator: Locator, opts: FetchOptions): Promise<import("@yarnpkg/fslib").ZipFS>;
}
PK
     @��&2zY  Y  ;   node_modules/@yarnpkg/plugin-http/lib/TarballHttpFetcher.js"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TarballHttpFetcher = void 0;
const core_1 = require("@yarnpkg/core");
const constants_1 = require("./constants");
class TarballHttpFetcher {
    supports(locator, opts) {
        if (!constants_1.TARBALL_REGEXP.test(locator.reference))
            return false;
        if (constants_1.PROTOCOL_REGEXP.test(locator.reference))
            return true;
        return false;
    }
    getLocalPath(locator, opts) {
        return null;
    }
    async fetch(locator, opts) {
        const expectedChecksum = opts.checksums.get(locator.locatorHash) || null;
        const [packageFs, releaseFs, checksum] = await opts.cache.fetchPackageFromCache(locator, expectedChecksum, {
            onHit: () => opts.report.reportCacheHit(locator),
            onMiss: () => opts.report.reportCacheMiss(locator, `${core_1.structUtils.prettyLocator(opts.project.configuration, locator)} can't be found in the cache and will be fetched from the remote server`),
            loader: () => this.fetchFromNetwork(locator, opts),
            skipIntegrityCheck: opts.skipIntegrityCheck,
            ...opts.cacheOptions,
        });
        return {
            packageFs,
            releaseFs,
            prefixPath: core_1.structUtils.getIdentVendorPath(locator),
            checksum,
        };
    }
    async fetchFromNetwork(locator, opts) {
        const sourceBuffer = await core_1.httpUtils.get(locator.reference, {
            configuration: opts.project.configuration,
        });
        return await core_1.tgzUtils.convertToZip(sourceBuffer, {
            compressionLevel: opts.project.configuration.get(`compressionLevel`),
            prefixPath: core_1.structUtils.getIdentVendorPath(locator),
            stripComponents: 1,
        });
    }
}
exports.TarballHttpFetcher = TarballHttpFetcher;
PK
     @����  �  >   node_modules/@yarnpkg/plugin-http/lib/TarballHttpResolver.d.tsimport { Resolver, ResolveOptions, MinimalResolveOptions } from '@yarnpkg/core';
import { Descriptor, Locator } from '@yarnpkg/core';
import { LinkType } from '@yarnpkg/core';
export declare class TarballHttpResolver implements Resolver {
    supportsDescriptor(descriptor: Descriptor, opts: MinimalResolveOptions): boolean;
    supportsLocator(locator: Locator, opts: MinimalResolveOptions): boolean;
    shouldPersistResolution(locator: Locator, opts: MinimalResolveOptions): boolean;
    bindDescriptor(descriptor: Descriptor, fromLocator: Locator, opts: MinimalResolveOptions): Descriptor;
    getResolutionDependencies(descriptor: Descriptor, opts: MinimalResolveOptions): never[];
    getCandidates(descriptor: Descriptor, dependencies: unknown, opts: ResolveOptions): Promise<Locator[]>;
    getSatisfying(descriptor: Descriptor, references: Array<string>, opts: ResolveOptions): Promise<null>;
    resolve(locator: Locator, opts: ResolveOptions): Promise<{
        version: string;
        languageName: string;
        linkType: LinkType;
        conditions: string | null;
        dependencies: Map<import("@yarnpkg/core").IdentHash, Descriptor>;
        peerDependencies: Map<import("@yarnpkg/core").IdentHash, Descriptor>;
        dependenciesMeta: Map<string, Map<string | null, import("@yarnpkg/core").DependencyMeta>>;
        peerDependenciesMeta: Map<string, import("@yarnpkg/core").PeerDependencyMeta>;
        bin: Map<string, import("@yarnpkg/fslib").PortablePath>;
        locatorHash: import("@yarnpkg/core").LocatorHash;
        reference: string;
        identHash: import("@yarnpkg/core").IdentHash;
        scope: string | null;
        name: string;
    }>;
}
PK
     @��r���M	  M	  <   node_modules/@yarnpkg/plugin-http/lib/TarballHttpResolver.js"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TarballHttpResolver = void 0;
const core_1 = require("@yarnpkg/core");
const core_2 = require("@yarnpkg/core");
const core_3 = require("@yarnpkg/core");
const constants_1 = require("./constants");
class TarballHttpResolver {
    supportsDescriptor(descriptor, opts) {
        if (!constants_1.TARBALL_REGEXP.test(descriptor.range))
            return false;
        if (constants_1.PROTOCOL_REGEXP.test(descriptor.range))
            return true;
        return false;
    }
    supportsLocator(locator, opts) {
        if (!constants_1.TARBALL_REGEXP.test(locator.reference))
            return false;
        if (constants_1.PROTOCOL_REGEXP.test(locator.reference))
            return true;
        return false;
    }
    shouldPersistResolution(locator, opts) {
        return true;
    }
    bindDescriptor(descriptor, fromLocator, opts) {
        return descriptor;
    }
    getResolutionDependencies(descriptor, opts) {
        return [];
    }
    async getCandidates(descriptor, dependencies, opts) {
        return [core_3.structUtils.convertDescriptorToLocator(descriptor)];
    }
    async getSatisfying(descriptor, references, opts) {
        return null;
    }
    async resolve(locator, opts) {
        if (!opts.fetchOptions)
            throw new Error(`Assertion failed: This resolver cannot be used unless a fetcher is configured`);
        const packageFetch = await opts.fetchOptions.fetcher.fetch(locator, opts.fetchOptions);
        const manifest = await core_3.miscUtils.releaseAfterUseAsync(async () => {
            return await core_1.Manifest.find(packageFetch.prefixPath, { baseFs: packageFetch.packageFs });
        }, packageFetch.releaseFs);
        return {
            ...locator,
            version: manifest.version || `0.0.0`,
            languageName: manifest.languageName || opts.project.configuration.get(`defaultLanguageName`),
            linkType: core_2.LinkType.HARD,
            conditions: manifest.getConditions(),
            dependencies: manifest.dependencies,
            peerDependencies: manifest.peerDependencies,
            dependenciesMeta: manifest.dependenciesMeta,
            peerDependenciesMeta: manifest.peerDependenciesMeta,
            bin: manifest.bin,
        };
    }
}
exports.TarballHttpResolver = TarballHttpResolver;
PK
     @����b�[   [   4   node_modules/@yarnpkg/plugin-http/lib/constants.d.tsexport declare const TARBALL_REGEXP: RegExp;
export declare const PROTOCOL_REGEXP: RegExp;
PK
     @���z�   �   2   node_modules/@yarnpkg/plugin-http/lib/constants.js"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PROTOCOL_REGEXP = exports.TARBALL_REGEXP = void 0;
exports.TARBALL_REGEXP = /^[^?]*\.(?:tar\.gz|tgz)(?:\?.*)?$/;
exports.PROTOCOL_REGEXP = /^https?:/;
PK
     @��#w��]   ]   0   node_modules/@yarnpkg/plugin-http/lib/index.d.tsimport { Plugin } from '@yarnpkg/core';
declare const plugin: Plugin;
export default plugin;
PK
     @��%����  �  .   node_modules/@yarnpkg/plugin-http/lib/index.js"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TarballHttpFetcher_1 = require("./TarballHttpFetcher");
const TarballHttpResolver_1 = require("./TarballHttpResolver");
const plugin = {
    fetchers: [
        TarballHttpFetcher_1.TarballHttpFetcher,
    ],
    resolvers: [
        TarballHttpResolver_1.TarballHttpResolver,
    ],
};
// eslint-disable-next-line arca/no-default-export
exports.default = plugin;
PK
     @���֓
    .   node_modules/@yarnpkg/plugin-http/package.json{
  "name": "@yarnpkg/plugin-http",
  "version": "2.2.0",
  "license": "BSD-2-Clause",
  "main": "./lib/index.js",
  "dependencies": {
    "@yarnpkg/fslib": "^2.6.0",
    "tslib": "^1.13.0"
  },
  "peerDependencies": {
    "@yarnpkg/core": "^3.1.0"
  },
  "devDependencies": {
    "@yarnpkg/core": "^3.1.0"
  },
  "repository": {
    "type": "git",
    "url": "ssh://git@github.com/yarnpkg/berry.git",
    "directory": "packages/plugin-http"
  },
  "scripts": {
    "postpack": "rm -rf lib",
    "prepack": "run build:compile \"$(pwd)\""
  },
  "publishConfig": {
    "main": "./lib/index.js",
    "typings": "./lib/index.d.ts"
  },
  "files": [
    "/lib/**/*"
  ],
  "engines": {
    "node": ">=12 <14 || 14.2 - 14.9 || >14.10.0"
  },
  "typings": "./lib/index.d.ts"
}PK?     @��                       �A    node_modules/PK?     @��                       �A+   node_modules/@yarnpkg/PK?     @��            "           �A_   node_modules/@yarnpkg/plugin-http/PK?
     @�����   �   +           ���   node_modules/@yarnpkg/plugin-http/README.mdPK?     @��            &           �A{  node_modules/@yarnpkg/plugin-http/lib/PK?
     @��i��p�  �  =           ���  node_modules/@yarnpkg/plugin-http/lib/TarballHttpFetcher.d.tsPK?
     @��&2zY  Y  ;           ���  node_modules/@yarnpkg/plugin-http/lib/TarballHttpFetcher.jsPK?
     @����  �  >           ���  node_modules/@yarnpkg/plugin-http/lib/TarballHttpResolver.d.tsPK?
     @��r���M	  M	  <           ��s  node_modules/@yarnpkg/plugin-http/lib/TarballHttpResolver.jsPK?
     @����b�[   [   4           ��  node_modules/@yarnpkg/plugin-http/lib/constants.d.tsPK?
     @���z�   �   2           ���  node_modules/@yarnpkg/plugin-http/lib/constants.jsPK?
     @��#w��]   ]   0           ��  node_modules/@yarnpkg/plugin-http/lib/index.d.tsPK?
     @��%����  �  .           ���  node_modules/@yarnpkg/plugin-http/lib/index.jsPK?
     @���֓
    .           ���!  node_modules/@yarnpkg/plugin-http/package.jsonPK      �  %    