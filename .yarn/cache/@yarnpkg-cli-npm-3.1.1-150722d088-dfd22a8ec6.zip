PK     @��               node_modules/PK     @��               node_modules/@yarnpkg/PK     @��               node_modules/@yarnpkg/cli/PK     @��               node_modules/@yarnpkg/cli/lib/PK
     @��K�r:�  �  -   node_modules/@yarnpkg/cli/lib/boot-cli-dev.jsconst fs = require(`fs`);

// Makes it possible to access our dependencies
const pnpFile = `${__dirname}/../../../.pnp.cjs`;
if (fs.existsSync(pnpFile))
  require(pnpFile).setup();

// Adds TS support to Node
require(`@yarnpkg/monorepo/scripts/setup-ts-execution`);

// Exposes the CLI version as like for the bundle
global.YARN_VERSION = `${require(`@yarnpkg/cli/package.json`).version}.dev`;

// Inject the plugins in the runtime. With Webpack that would be through
// val-loader which would execute pluginConfiguration.raw.js, so in Node
// we need to do something similar and mutate the require cache.
const PLUGIN_CONFIG_MODULE = `./tools/getPluginConfiguration.ts`;
require.cache[require.resolve(PLUGIN_CONFIG_MODULE)] = {exports: {getPluginConfiguration}};

const micromatch = require(`micromatch`);

module.exports = require(`./cli`);

function getPluginConfiguration() {
  const folders = fs.readdirSync(`${__dirname}/../../`);

  const pluginFolders = folders.filter(folder => {
    if (!folder.startsWith(`plugin-`))
      return false;

    if (process.env.BLACKLIST && micromatch.match([folder, folder.replace(`plugin-`, ``)], process.env.BLACKLIST).length > 0) {
      console.warn(`Disabled blacklisted plugin ${folder}`);
      return false;
    }

    let isRequirable;
    try {
      require(`${__dirname}/../../${folder}`);
      isRequirable = true;
    } catch (e) {
      console.warn(`Disabled non-requirable plugin ${folder}: ${e.message}`);
      isRequirable = false;
    }
    return isRequirable;
  });

  // Note that we don't need to populate the `modules` field, because the
  // plugins will be loaded without being transformed by the builder wrapper,
  // so they will simply access their own set of dependencies.
  const pluginConfiguration = {
    plugins: new Set(),
    modules: new Map(),
  };

  for (const folder of pluginFolders) {
    pluginConfiguration.plugins.add(`@yarnpkg/${folder}`);
    pluginConfiguration.modules.set(`@yarnpkg/${folder}`, require(`../../${folder}`));
  }

  const {getDynamicLibs} = require(`./tools/getDynamicLibs`);
  for (const [name, module] of getDynamicLibs())
    pluginConfiguration.modules.set(name, module);

  return pluginConfiguration;
}
PK
     @��3��k�  �  (   node_modules/@yarnpkg/cli/lib/index.d.tsexport { BaseCommand } from './tools/BaseCommand';
export { WorkspaceRequiredError } from './tools/WorkspaceRequiredError';
export { getDynamicLibs } from './tools/getDynamicLibs';
export { getPluginConfiguration } from './tools/getPluginConfiguration';
export { openWorkspace } from './tools/openWorkspace';
export { main } from './main';
export { pluginCommands } from './pluginCommands';
PK
     @���U!7D  D  &   node_modules/@yarnpkg/cli/lib/index.js"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.pluginCommands = exports.main = exports.openWorkspace = exports.getPluginConfiguration = exports.getDynamicLibs = exports.WorkspaceRequiredError = exports.BaseCommand = void 0;
var BaseCommand_1 = require("./tools/BaseCommand");
Object.defineProperty(exports, "BaseCommand", { enumerable: true, get: function () { return BaseCommand_1.BaseCommand; } });
var WorkspaceRequiredError_1 = require("./tools/WorkspaceRequiredError");
Object.defineProperty(exports, "WorkspaceRequiredError", { enumerable: true, get: function () { return WorkspaceRequiredError_1.WorkspaceRequiredError; } });
var getDynamicLibs_1 = require("./tools/getDynamicLibs");
Object.defineProperty(exports, "getDynamicLibs", { enumerable: true, get: function () { return getDynamicLibs_1.getDynamicLibs; } });
var getPluginConfiguration_1 = require("./tools/getPluginConfiguration");
Object.defineProperty(exports, "getPluginConfiguration", { enumerable: true, get: function () { return getPluginConfiguration_1.getPluginConfiguration; } });
var openWorkspace_1 = require("./tools/openWorkspace");
Object.defineProperty(exports, "openWorkspace", { enumerable: true, get: function () { return openWorkspace_1.openWorkspace; } });
var main_1 = require("./main");
Object.defineProperty(exports, "main", { enumerable: true, get: function () { return main_1.main; } });
var pluginCommands_1 = require("./pluginCommands");
Object.defineProperty(exports, "pluginCommands", { enumerable: true, get: function () { return pluginCommands_1.pluginCommands; } });
PK
     @��?���   �   '   node_modules/@yarnpkg/cli/lib/main.d.tsimport { PluginConfiguration } from '@yarnpkg/core';
export declare function main({ binaryVersion, pluginConfiguration }: {
    binaryVersion: string;
    pluginConfiguration: PluginConfiguration;
}): Promise<void>;
PK
     @��>ܙ    %   node_modules/@yarnpkg/cli/lib/main.js"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.main = void 0;
const core_1 = require("@yarnpkg/core");
const fslib_1 = require("@yarnpkg/fslib");
const child_process_1 = require("child_process");
const ci_info_1 = require("ci-info");
const clipanion_1 = require("clipanion");
const fs_1 = require("fs");
const pluginCommands_1 = require("./pluginCommands");
function runBinary(path) {
    const physicalPath = fslib_1.npath.fromPortablePath(path);
    process.on(`SIGINT`, () => {
        // We don't want SIGINT to kill our process; we want it to kill the
        // innermost process, whose end will cause our own to exit.
    });
    if (physicalPath) {
        (0, child_process_1.execFileSync)(process.execPath, [physicalPath, ...process.argv.slice(2)], {
            stdio: `inherit`,
            env: {
                ...process.env,
                YARN_IGNORE_PATH: `1`,
                YARN_IGNORE_CWD: `1`,
            },
        });
    }
    else {
        (0, child_process_1.execFileSync)(physicalPath, process.argv.slice(2), {
            stdio: `inherit`,
            env: {
                ...process.env,
                YARN_IGNORE_PATH: `1`,
                YARN_IGNORE_CWD: `1`,
            },
        });
    }
}
async function main({ binaryVersion, pluginConfiguration }) {
    async function run() {
        const cli = new clipanion_1.Cli({
            binaryLabel: `Yarn Package Manager`,
            binaryName: `yarn`,
            binaryVersion,
        });
        try {
            await exec(cli);
        }
        catch (error) {
            process.stdout.write(cli.error(error));
            process.exitCode = 1;
        }
    }
    async function exec(cli) {
        // Non-exhaustive known requirements:
        // - 14.0 and 14.1 empty http responses - https://github.com/sindresorhus/got/issues/1496
        // - 14.10.0 broken streams - https://github.com/nodejs/node/pull/34035 (fix: https://github.com/nodejs/node/commit/0f94c6b4e4)
        var _a, _b, _c, _d, _e;
        const version = process.versions.node;
        const range = `>=12 <14 || 14.2 - 14.9 || >14.10.0`;
        if (process.env.YARN_IGNORE_NODE !== `1` && !core_1.semverUtils.satisfiesWithPrereleases(version, range))
            throw new clipanion_1.UsageError(`This tool requires a Node version compatible with ${range} (got ${version}). Upgrade Node, or set \`YARN_IGNORE_NODE=1\` in your environment.`);
        // Since we only care about a few very specific settings (yarn-path and ignore-path) we tolerate extra configuration key.
        // If we didn't, we wouldn't even be able to run `yarn config` (which is recommended in the invalid config error message)
        const configuration = await core_1.Configuration.find(fslib_1.npath.toPortablePath(process.cwd()), pluginConfiguration, {
            usePath: true,
            strict: false,
        });
        const yarnPath = configuration.get(`yarnPath`);
        const ignorePath = configuration.get(`ignorePath`);
        const ignoreCwd = configuration.get(`ignoreCwd`);
        const selfPath = fslib_1.npath.toPortablePath(fslib_1.npath.resolve(process.argv[1]));
        const tryRead = (p) => fslib_1.xfs.readFilePromise(p).catch(() => {
            return Buffer.of();
        });
        const isSameBinary = async () => yarnPath === selfPath ||
            Buffer.compare(...await Promise.all([
                tryRead(yarnPath),
                tryRead(selfPath),
            ])) === 0;
        // Avoid unnecessary spawn when run directly
        if (!ignorePath && !ignoreCwd && await isSameBinary()) {
            process.env.YARN_IGNORE_PATH = `1`;
            process.env.YARN_IGNORE_CWD = `1`;
            await exec(cli);
            return;
        }
        else if (yarnPath !== null && !ignorePath) {
            if (!fslib_1.xfs.existsSync(yarnPath)) {
                process.stdout.write(cli.error(new Error(`The "yarn-path" option has been set (in ${configuration.sources.get(`yarnPath`)}), but the specified location doesn't exist (${yarnPath}).`)));
                process.exitCode = 1;
            }
            else {
                try {
                    runBinary(yarnPath);
                }
                catch (error) {
                    process.exitCode = error.code || 1;
                }
            }
        }
        else {
            if (ignorePath)
                delete process.env.YARN_IGNORE_PATH;
            const isTelemetryEnabled = configuration.get(`enableTelemetry`);
            if (isTelemetryEnabled && !ci_info_1.isCI && process.stdout.isTTY)
                core_1.Configuration.telemetry = new core_1.TelemetryManager(configuration, `puba9cdc10ec5790a2cf4969dd413a47270`);
            (_a = core_1.Configuration.telemetry) === null || _a === void 0 ? void 0 : _a.reportVersion(binaryVersion);
            for (const [name, plugin] of configuration.plugins.entries()) {
                if (pluginCommands_1.pluginCommands.has((_c = (_b = name.match(/^@yarnpkg\/plugin-(.*)$/)) === null || _b === void 0 ? void 0 : _b[1]) !== null && _c !== void 0 ? _c : ``))
                    (_d = core_1.Configuration.telemetry) === null || _d === void 0 ? void 0 : _d.reportPluginName(name);
                for (const command of plugin.commands || []) {
                    cli.register(command);
                }
            }
            const command = cli.process(process.argv.slice(2));
            if (!command.help)
                (_e = core_1.Configuration.telemetry) === null || _e === void 0 ? void 0 : _e.reportCommandName(command.path.join(` `));
            // @ts-expect-error: The cwd is a global option defined by BaseCommand
            const cwd = command.cwd;
            if (typeof cwd !== `undefined` && !ignoreCwd) {
                const iAmHere = (0, fs_1.realpathSync)(process.cwd());
                const iShouldBeHere = (0, fs_1.realpathSync)(cwd);
                if (iAmHere !== iShouldBeHere) {
                    process.chdir(cwd);
                    await run();
                    return;
                }
            }
            await cli.runExit(command, {
                cwd: fslib_1.npath.toPortablePath(process.cwd()),
                plugins: pluginConfiguration,
                quiet: false,
                stdin: process.stdin,
                stdout: process.stdout,
                stderr: process.stderr,
            });
        }
    }
    return run()
        .catch(error => {
        process.stdout.write(error.stack || error.message);
        process.exitCode = 1;
    })
        .finally(() => fslib_1.xfs.rmtempPromise());
}
exports.main = main;
PK
     @��z�|>   >   1   node_modules/@yarnpkg/cli/lib/pluginCommands.d.tsexport declare const pluginCommands: Map<string, string[][]>;
PK
     @���5�"  "  /   node_modules/@yarnpkg/cli/lib/pluginCommands.js"use strict";
// Don't modify this script directly! Instead, run:
// yarn build:plugin-commands
Object.defineProperty(exports, "__esModule", { value: true });
exports.pluginCommands = void 0;
exports.pluginCommands = new Map([
    [`constraints`, [
            [`constraints`, `query`],
            [`constraints`, `source`],
            [`constraints`],
        ]],
    [`exec`, []],
    [`interactive-tools`, [
            [`search`],
            [`upgrade-interactive`],
        ]],
    [`stage`, [
            [`stage`],
        ]],
    [`typescript`, []],
    [`version`, [
            [`version`, `apply`],
            [`version`, `check`],
            [`version`],
        ]],
    [`workspace-tools`, [
            [`workspaces`, `focus`],
            [`workspaces`, `foreach`],
        ]],
]);
PK
     @��QD@      ,   node_modules/@yarnpkg/cli/lib/polyfills.d.tsexport {};
PK
     @������r   r   *   node_modules/@yarnpkg/cli/lib/polyfills.js"use strict";
// No polyfills are currently needed
Object.defineProperty(exports, "__esModule", { value: true });
PK     @��            $   node_modules/@yarnpkg/cli/lib/tools/PK
     @��r#ΐ�   �   4   node_modules/@yarnpkg/cli/lib/tools/BaseCommand.d.tsimport { CommandContext } from '@yarnpkg/core';
import { Command } from 'clipanion';
export declare abstract class BaseCommand extends Command<CommandContext> {
    cwd: string | undefined;
    abstract execute(): Promise<number | void>;
}
PK
     @����5=j  j  2   node_modules/@yarnpkg/cli/lib/tools/BaseCommand.js"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseCommand = void 0;
const clipanion_1 = require("clipanion");
class BaseCommand extends clipanion_1.Command {
    constructor() {
        super(...arguments);
        this.cwd = clipanion_1.Option.String(`--cwd`, { hidden: true });
    }
}
exports.BaseCommand = BaseCommand;
PK
     @��eS��   �   ?   node_modules/@yarnpkg/cli/lib/tools/WorkspaceRequiredError.d.tsimport { PortablePath } from '@yarnpkg/fslib';
import { UsageError } from 'clipanion';
export declare class WorkspaceRequiredError extends UsageError {
    constructor(projectCwd: PortablePath, cwd: PortablePath);
}
PK
     @���{�  �  =   node_modules/@yarnpkg/cli/lib/tools/WorkspaceRequiredError.js"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WorkspaceRequiredError = void 0;
const core_1 = require("@yarnpkg/core");
const fslib_1 = require("@yarnpkg/fslib");
const clipanion_1 = require("clipanion");
class WorkspaceRequiredError extends clipanion_1.UsageError {
    constructor(projectCwd, cwd) {
        const relativePath = fslib_1.ppath.relative(projectCwd, cwd);
        const manifestPath = fslib_1.ppath.join(projectCwd, core_1.Manifest.fileName);
        super(`This command can only be run from within a workspace of your project (${relativePath} isn't a workspace of ${manifestPath}).`);
    }
}
exports.WorkspaceRequiredError = WorkspaceRequiredError;
PK
     @����^M   M   G   node_modules/@yarnpkg/cli/lib/tools/backportClipanionCompatibility.d.tsexport declare function backportClipanionCompatibility(clipanion: any): any;
PK
     @���BW��  �  E   node_modules/@yarnpkg/cli/lib/tools/backportClipanionCompatibility.js"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.backportClipanionCompatibility = void 0;
function backportClipanionCompatibility(clipanion) {
    clipanion.Command.Path = (...p) => (instance) => {
        instance.paths = instance.paths || [];
        instance.paths.push(p);
    };
    for (const fn of [`Array`, `Boolean`, `String`, `Proxy`, `Rest`, `Counter`]) {
        clipanion.Command[fn] = (...args) => (instance, propertyName) => {
            const value = clipanion.Option[fn](...args);
            Object.defineProperty(instance, `__${propertyName}`, {
                configurable: false,
                enumerable: true,
                get() {
                    return value;
                },
                set(value) {
                    this[propertyName] = value;
                },
            });
        };
    }
    return clipanion;
}
exports.backportClipanionCompatibility = backportClipanionCompatibility;
PK
     @����`�=   =   7   node_modules/@yarnpkg/cli/lib/tools/getDynamicLibs.d.tsexport declare const getDynamicLibs: () => Map<string, any>;
PK
     @��]l�M�  �  5   node_modules/@yarnpkg/cli/lib/tools/getDynamicLibs.js"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getDynamicLibs = void 0;
const tslib_1 = require("tslib");
const cli = (0, tslib_1.__importStar)(require("@yarnpkg/cli"));
const core = (0, tslib_1.__importStar)(require("@yarnpkg/core"));
const fslib = (0, tslib_1.__importStar)(require("@yarnpkg/fslib"));
const libzip = (0, tslib_1.__importStar)(require("@yarnpkg/libzip"));
const parsers = (0, tslib_1.__importStar)(require("@yarnpkg/parsers"));
const shell = (0, tslib_1.__importStar)(require("@yarnpkg/shell"));
const clipanion = (0, tslib_1.__importStar)(require("clipanion"));
const semver = (0, tslib_1.__importStar)(require("semver"));
const typanion = (0, tslib_1.__importStar)(require("typanion"));
const yup = (0, tslib_1.__importStar)(require("yup"));
const getDynamicLibs = () => new Map([
    [`@yarnpkg/cli`, cli],
    [`@yarnpkg/core`, core],
    [`@yarnpkg/fslib`, fslib],
    [`@yarnpkg/libzip`, libzip],
    [`@yarnpkg/parsers`, parsers],
    [`@yarnpkg/shell`, shell],
    // Those ones are always useful
    [`clipanion`, clipanion],
    [`semver`, semver],
    [`typanion`, typanion],
    // TODO: remove in next major
    [`yup`, yup],
]);
exports.getDynamicLibs = getDynamicLibs;
PK
     @�����B|   |   ?   node_modules/@yarnpkg/cli/lib/tools/getPluginConfiguration.d.tsimport { PluginConfiguration } from '@yarnpkg/core';
export declare function getPluginConfiguration(): PluginConfiguration;
PK
     @����6B�  �  =   node_modules/@yarnpkg/cli/lib/tools/getPluginConfiguration.js"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getPluginConfiguration = void 0;
const tslib_1 = require("tslib");
// @ts-expect-error
const package_json_1 = (0, tslib_1.__importDefault)(require("@yarnpkg/cli/package.json"));
const getDynamicLibs_1 = require("./getDynamicLibs");
function getPluginConfiguration() {
    const plugins = new Set();
    for (const dependencyName of package_json_1.default[`@yarnpkg/builder`].bundles.standard)
        plugins.add(dependencyName);
    const modules = (0, getDynamicLibs_1.getDynamicLibs)();
    for (const plugin of plugins)
        modules.set(plugin, require(plugin).default);
    return { plugins, modules };
}
exports.getPluginConfiguration = getPluginConfiguration;
PK
     @�����/  /  A   node_modules/@yarnpkg/cli/lib/tools/getPluginConfiguration.val.js// Note that this file isn't the real export - it is run at build-time and its
// return value is what's used within the bundle (cf val-loader).

module.exports = ({modules, plugins}) => {
  const importSegment = modules.map((request, index) => {
    return `import * as _${index} from ${JSON.stringify(request)};\n`;
  }).join(``);

  const moduleSegment = `  modules: new Map([\n${modules.map((request, index) => {
    return `    [${JSON.stringify(require(`${request}/package.json`).name)}, ${request === `clipanion` ? `backportClipanionCompatibility` : ``}(_${index})],\n`;
  }).join(``)}  ]),\n`;

  const pluginSegment = `  plugins: new Set([\n${plugins.map(request => {
    return `    ${JSON.stringify(require(`${request}/package.json`).name)},\n`;
  }).join(``)}  ]),\n`;

  return {
    code: [
      `import {backportClipanionCompatibility} from './backportClipanionCompatibility';\n`,
      `\n`,
      importSegment,
      `export const getPluginConfiguration = () => ({\n`,
      moduleSegment,
      pluginSegment,
      `});\n`,
    ].join(`\n`),
  };
};
PK
     @����-a�   �   6   node_modules/@yarnpkg/cli/lib/tools/openWorkspace.d.tsimport { Configuration } from '@yarnpkg/core';
import { PortablePath } from '@yarnpkg/fslib';
export declare function openWorkspace(configuration: Configuration, cwd: PortablePath): Promise<import("@yarnpkg/core").Workspace>;
PK
     @��#���	  	  4   node_modules/@yarnpkg/cli/lib/tools/openWorkspace.js"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.openWorkspace = void 0;
const core_1 = require("@yarnpkg/core");
const WorkspaceRequiredError_1 = require("./WorkspaceRequiredError");
async function openWorkspace(configuration, cwd) {
    const { project, workspace } = await core_1.Project.find(configuration, cwd);
    if (!workspace)
        throw new WorkspaceRequiredError_1.WorkspaceRequiredError(project.cwd, cwd);
    return workspace;
}
exports.openWorkspace = openWorkspace;
PK
     @��K���
  �
  &   node_modules/@yarnpkg/cli/package.json{
  "name": "@yarnpkg/cli",
  "version": "3.1.1",
  "license": "BSD-2-Clause",
  "main": "./lib/index.js",
  "dependencies": {
    "@yarnpkg/core": "^3.1.0",
    "@yarnpkg/fslib": "^2.6.0",
    "@yarnpkg/libzip": "^2.2.2",
    "@yarnpkg/parsers": "^2.4.1",
    "@yarnpkg/plugin-compat": "^3.1.1",
    "@yarnpkg/plugin-dlx": "^3.1.0",
    "@yarnpkg/plugin-essentials": "^3.1.0",
    "@yarnpkg/plugin-file": "^2.3.0",
    "@yarnpkg/plugin-git": "^2.5.0",
    "@yarnpkg/plugin-github": "^2.3.0",
    "@yarnpkg/plugin-http": "^2.2.0",
    "@yarnpkg/plugin-init": "^3.1.0",
    "@yarnpkg/plugin-link": "^2.2.0",
    "@yarnpkg/plugin-nm": "^3.1.0",
    "@yarnpkg/plugin-npm": "^2.6.0",
    "@yarnpkg/plugin-npm-cli": "^3.1.0",
    "@yarnpkg/plugin-pack": "^3.1.0",
    "@yarnpkg/plugin-patch": "^3.1.0",
    "@yarnpkg/plugin-pnp": "^3.1.0",
    "@yarnpkg/plugin-pnpm": "^1.0.0",
    "@yarnpkg/shell": "^3.1.0",
    "chalk": "^3.0.0",
    "ci-info": "^3.2.0",
    "clipanion": "^3.0.1",
    "semver": "^7.1.2",
    "tslib": "^1.13.0",
    "typanion": "^3.3.0",
    "yup": "^0.32.9"
  },
  "devDependencies": {
    "@types/semver": "^7.1.0",
    "@types/yup": "^0",
    "@yarnpkg/builder": "^3.1.0",
    "@yarnpkg/monorepo": "^0.0.0",
    "@yarnpkg/pnpify": "^3.1.0",
    "micromatch": "^4.0.2",
    "typescript": "^4.5.2"
  },
  "peerDependencies": {
    "@yarnpkg/core": "^3.1.0"
  },
  "scripts": {
    "postpack": "rm -rf lib",
    "prepack": "run build:compile \"$(pwd)\"",
    "build:cli+hook": "run build:pnp:hook && builder build bundle",
    "build:cli": "builder build bundle",
    "run:cli": "builder run",
    "update-local": "run build:cli --no-git-hash && rsync -a --delete bundles/ bin/"
  },
  "publishConfig": {
    "main": "./lib/index.js",
    "types": "./lib/index.d.ts",
    "bin": null
  },
  "files": [
    "/lib/**/*",
    "!/lib/pluginConfiguration.*",
    "!/lib/cli.*"
  ],
  "@yarnpkg/builder": {
    "bundles": {
      "standard": [
        "@yarnpkg/plugin-essentials",
        "@yarnpkg/plugin-compat",
        "@yarnpkg/plugin-dlx",
        "@yarnpkg/plugin-file",
        "@yarnpkg/plugin-git",
        "@yarnpkg/plugin-github",
        "@yarnpkg/plugin-http",
        "@yarnpkg/plugin-init",
        "@yarnpkg/plugin-link",
        "@yarnpkg/plugin-nm",
        "@yarnpkg/plugin-npm",
        "@yarnpkg/plugin-npm-cli",
        "@yarnpkg/plugin-pack",
        "@yarnpkg/plugin-patch",
        "@yarnpkg/plugin-pnp",
        "@yarnpkg/plugin-pnpm"
      ]
    }
  },
  "repository": {
    "type": "git",
    "url": "ssh://git@github.com/yarnpkg/berry.git",
    "directory": "packages/yarnpkg-cli"
  },
  "engines": {
    "node": ">=12 <14 || 14.2 - 14.9 || >14.10.0"
  },
  "types": "./lib/index.d.ts"
}PK?     @��                       �A    node_modules/PK?     @��                       �A+   node_modules/@yarnpkg/PK?     @��                       �A_   node_modules/@yarnpkg/cli/PK?     @��                       �A�   node_modules/@yarnpkg/cli/lib/PK?
     @��K�r:�  �  -           ���   node_modules/@yarnpkg/cli/lib/boot-cli-dev.jsPK?
     @��3��k�  �  (           ���	  node_modules/@yarnpkg/cli/lib/index.d.tsPK?
     @���U!7D  D  &           ���  node_modules/@yarnpkg/cli/lib/index.jsPK?
     @��?���   �   '           ��  node_modules/@yarnpkg/cli/lib/main.d.tsPK?
     @��>ܙ    %           ��<  node_modules/@yarnpkg/cli/lib/main.jsPK?
     @��z�|>   >   1           ���-  node_modules/@yarnpkg/cli/lib/pluginCommands.d.tsPK?
     @���5�"  "  /           ��+.  node_modules/@yarnpkg/cli/lib/pluginCommands.jsPK?
     @��QD@      ,           ���1  node_modules/@yarnpkg/cli/lib/polyfills.d.tsPK?
     @������r   r   *           ���1  node_modules/@yarnpkg/cli/lib/polyfills.jsPK?     @��            $           �A�2  node_modules/@yarnpkg/cli/lib/tools/PK?
     @��r#ΐ�   �   4           ���2  node_modules/@yarnpkg/cli/lib/tools/BaseCommand.d.tsPK?
     @����5=j  j  2           ��-4  node_modules/@yarnpkg/cli/lib/tools/BaseCommand.jsPK?
     @��eS��   �   ?           ���5  node_modules/@yarnpkg/cli/lib/tools/WorkspaceRequiredError.d.tsPK?
     @���{�  �  =           ��7  node_modules/@yarnpkg/cli/lib/tools/WorkspaceRequiredError.jsPK?
     @����^M   M   G           ��9:  node_modules/@yarnpkg/cli/lib/tools/backportClipanionCompatibility.d.tsPK?
     @���BW��  �  E           ���:  node_modules/@yarnpkg/cli/lib/tools/backportClipanionCompatibility.jsPK?
     @����`�=   =   7           ��?  node_modules/@yarnpkg/cli/lib/tools/getDynamicLibs.d.tsPK?
     @��]l�M�  �  5           ���?  node_modules/@yarnpkg/cli/lib/tools/getDynamicLibs.jsPK?
     @�����B|   |   ?           ���D  node_modules/@yarnpkg/cli/lib/tools/getPluginConfiguration.d.tsPK?
     @����6B�  �  =           ���E  node_modules/@yarnpkg/cli/lib/tools/getPluginConfiguration.jsPK?
     @�����/  /  A           ��I  node_modules/@yarnpkg/cli/lib/tools/getPluginConfiguration.val.jsPK?
     @����-a�   �   6           ���M  node_modules/@yarnpkg/cli/lib/tools/openWorkspace.d.tsPK?
     @��#���	  	  4           ���N  node_modules/@yarnpkg/cli/lib/tools/openWorkspace.jsPK?
     @��K���
  �
  &           ��!Q  node_modules/@yarnpkg/cli/package.jsonPK      +
  \    